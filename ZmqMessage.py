import zmq
from typing import Optional, List
from dataclasses import dataclass, field
from uuid import UUID

@dataclass
class ZmqMessage:
    address : str
    command : str
    args : Optional[List] = field(default=None)
    data : Optional[List] = field(default=None)
    uuid : Optional[UUID] = field(default=None)
    origin : Optional[str] = field(default=None)

    # the context of the function/method call is object 'target'
    def run_command(self, target):
        cmd = getattr(target, self.command)
        return cmd(*self.args)
