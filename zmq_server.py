import time
import zmq

context = zmq.Context()

frontend = context.socket(zmq.XPUB)
frontend.bind("tcp://*:5559")

backend = context.socket(zmq.XSUB)
backend.bind("tcp://*:5560")

print("running zmq.proxy...")
zmq.proxy(frontend, backend)

frontend.close()
backend.close()
context.term()
