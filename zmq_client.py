import zmq
from threading import Thread, Event, Lock
import queue
import uuid


class CommunicationHandler():
    def __init__(self, address, on_receive):
        self.stop_event = Event()
        self.q = queue.Queue()

        self.context = zmq.Context()
        self.tx_thread = Thread(target=self._transmitter)
        self.tx_thread.start()

        self.rx_thread = Thread(target=self._receiver)
        self.rx_thread.start()

        self.notifications = {}
        self.notifications_data = {}
        self.address = address
        self.on_receive = on_receive

        if True:
            self.q_debug = queue.Queue()
            self.lock = Lock()

    def q_put(self, message):
        self.lock.acquire()
        self.q_debug.put(message)
        self.lock.release()

    def send(self, msg, callback=None):
        msg.uuid = uuid.uuid4()
        msg.origin = self.address
        self.notifications[msg.uuid] = Event()
        self.q.put(msg)

        Thread(target=self._notifier, args=(msg.uuid, callback)).start()

    def stop(self):
        self.stop_signal.set()

    def _transmitter(self):
        tx = self.context.socket(zmq.PUB)
        tx.connect("tcp://localhost:5560")

        while not self.stop_event.wait(0):
            try:
                msg = self.q.get(timeout=1)
                self.q_put(f"TX: sending message {msg.address}...")
                tx.send_pyobj(msg)
            except queue.Empty:
                pass

    def _receiver(self):
        rx = self.context.socket(zmq.SUB)
        rx.connect("tcp://localhost:5559")
        rx.setsockopt(zmq.SUBSCRIBE, b"")  # subscribe to all messages

        while not self.stop_event.wait(0):
           msg = rx.recv_pyobj()
           if self.address == msg.address:
               self.q_put(f"RX: received command '{msg.command}' addressed to '{msg.address}'!")
               self.q_put(msg.uuid) # DEBUG
               self.q_put(self.notifications) # DEBUG
               if msg.uuid in self.notifications and msg.origin == self.address:
                   # was waiting for a reply
                   self.q_put('WAS WAITING FOR REPLY') # DEBUG
                   self.notifications_data[msg.uuid] = msg
                   self.notifications[msg.uuid].set()  # signal waiting thread message arrived
               else:
                   self.q_put('sending ACKNOWLEDGE') # DEBUG
                   # other application requests us to do something
                   msg.data = self.on_receive(msg)
                   msg.address, msg.origin = msg.origin, msg.address
                   msg.command = "ACKNOWLEDGE"
                   #self.q.put(msg)

           elif self.address == "":
               # TODO: handle broadcast, potentially with separate callback
               continue
           else:
               continue

    def _notifier(self, uuid, callback):
        self.notifications[uuid].wait()
        self.q_put('notified!')
        if callback:
            self.q_put('called back') # DEBUG
            callback(self.notifications_data[uuid])

        # prevent dictionary from growing
        self.notifications.pop(uuid)
        self.notifications_data.pop(uuid)
