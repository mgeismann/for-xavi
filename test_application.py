from ZmqMessage import ZmqMessage
import time
from zmq_client import CommunicationHandler
import queue
import threading

class Test:
    def foo(self, msg):
        print(f"Received reply! {msg.data} {msg.command} {msg.address} {msg.origin}")

    def get_funding(self, currency):
        return "1 billion " + currency

    def on_receive(self, msg):
        msg.run_command(self)

    def __init__(self):
        self.lock = threading.Lock()
        self.comm = CommunicationHandler("", self.on_receive)
        #self.comm2 = CommunicationHandler("SciLifeLab2", self.on_receive)

        time.sleep(1)
        msg = ZmqMessage(f"SciLifeLab1", "get_funding", ["Dollars"])
        self.comm.send(msg, self.foo)
        # time.sleep(1)
        # msg = ZmqMessage(f"SciLifeLab2", "get_funding", ["Euros"])
        # self.comm.send(msg, self.foo)
        time.sleep(1)
        # TODO: test request-repl

    def stop(self):
        comm.stop()
        comm2.stop()

def print_queue(q):
    while not q.empty():
        print('ITEM:', q.get())

a = Test()
time.sleep(3)

print_queue(a.comm.q_debug)
print('---------------------------')
#print_queue(a.comm2.q_debug)
Test.stop()
